#!/bin/bash

# Updating and cleaning
yum update -y
yum autoremove -y
yum clean all

# Installing Vagrant default key
mkdir -pm 700 /home/vagrant/.ssh
curl -sL https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub -o /home/vagrant/.ssh/authorized_keys
chmod 0600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant:vagrant /home/vagrant/.ssh

# Removing temporary files
rm -rf /home/vagrant/linux-$(uname -r | cut -d. -f1-3)
rm  -f /home/vagrant/VBoxGuestAdditions.iso
rm -rf /tmp/*
rm -rf /root/*
rm  -f /var/log/wtmp /var/log/btmp /var/log/vboxadd-install.log
rm -rf /var/cache/* /usr/share/doc/*
rm -rf /var/cache/yum
rm  -f ~/.bash_history
rm -rf /run/log/journal/*
history -c

# Filling up the whole empty space with zeros
dd if=/dev/zero of=/EMPTY bs=1M
rm -f /EMPTY
sync

# Finishing
echo "Finishing..."

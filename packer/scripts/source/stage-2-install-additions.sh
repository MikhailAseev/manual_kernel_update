#!/bin/bash

# Installing VirtualBox Linux Guest Additions
mount VBoxGuestAdditions.iso /mnt
cd /mnt
sh ./VBoxLinuxAdditions.run --nox11 && 
echo "VirtualBox Guest Additions built and installed for the new Linux kernel"
cd ~
umount /mnt

# Rebooting
echo "Rebooting" &&
reboot

#!/bin/bash

# Bash variables
old_kernel_version=$(uname -r | cut -d. -f1,2)
new_kernel_version=5.17.2
gcc_version=9

# Installing necessary packages
yum update -y
yum install -y centos-release-scl 
yum install -y rsync ncurses-devel make gcc perl bc bison flex elfutils-libelf-devel openssl-devel grub2 devtoolset-$gcc_version dkms grubby rpm-build

# Enabling newer GCC compiler
echo "source /opt/rh/devtoolset-$gcc_version/enable" >> /etc/bashrc
source /etc/bashrc

# Downloading and unarchiving new kernel
curl -O https://cdn.kernel.org/pub/linux/kernel/v$(echo $new_kernel_version | cut -d. -f1).x/linux-$new_kernel_version.tar.xz
tar -xJvf linux-$new_kernel_version.tar.xz
rm linux-$new_kernel_version.tar.xz

# Building new kernel
cd linux-$new_kernel_version/
cp -v $(ls -t /boot/config* | tail -n1) .config &&
make olddefconfig &&
make -j$(nproc) rpm-pkg &&
echo "New Linux kernel built"

# Installing newly built kernel (including rescue) and kernel headers
rm -f /boot/*rescue*
cd ~
rpm -iUv rpmbuild/RPMS/x86_64/*.rpm &&
echo "New Linux kernel installed. Reboot needed"
rm -rf rpmbuild

# Removing older kernels (Only for current demo! Not for production!)
yum remove kernel-debug-devel kernel-tools kernel-tools-libs -y
rm -rf /usr/src/kernels/*$old_kernel_version*
rm -rf /usr/src/debug/*$old_kernel_version*
rm -r /boot/*$old_kernel_version*

# Updating GRUB
grub2-mkconfig -o /boot/grub2/grub.cfg &&
grub2-set-default 0 &&
echo "GRUB update done"

# Rebooting
echo "Rebooting..." &&
reboot

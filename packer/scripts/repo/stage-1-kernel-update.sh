#!/bin/bash

# Saving old kernel version
uname -r | cut -d. -f1,2 > old_kernel_version.txt

# Installing ELRepo
yum install -y http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm

# Removing old rescue kernel
rm -f /boot/*rescue*

# Installing new kernel from repo
yum --enablerepo elrepo-kernel install kernel-ml -y

# Updating GRUB
grub2-mkconfig -o /boot/grub2/grub.cfg &&
grub2-set-default 0 &&
echo "GRUB update done"

# Rebooting VM
shutdown -r now

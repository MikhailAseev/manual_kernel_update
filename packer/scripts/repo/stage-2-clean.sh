#!/bin/bash

# Updating
yum update -y

# Removing older kernels (Only for current demo! Not for production!)
yum remove -y kernel kernel-tools kernel-tools-libs  
rm -f /boot/*$(cat old_kernel_version.txt)*

# Updating GRUB
grub2-mkconfig -o /boot/grub2/grub.cfg &&
grub2-set-default 0 &&
echo "GRUB update done"

# Cleaning
yum autoremove -y
yum clean all

# Installing Vagrant default key
mkdir -pm 700 /home/vagrant/.ssh
curl -sL https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub -o /home/vagrant/.ssh/authorized_keys
chmod 0600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant:vagrant /home/vagrant/.ssh

# Removing temporary files
rm  -f /home/vagrant/*
rm -rf /tmp/*
rm -rf /root/*
rm  -f /var/log/wtmp /var/log/btmp
rm -rf /var/cache/* /usr/share/doc/*
rm -rf /var/cache/yum
rm  -f ~/.bash_history
rm -rf /run/log/journal/*
history -c

# Filling up the whole empty space with zeros
dd if=/dev/zero of=/EMPTY bs=1M
rm -f /EMPTY
sync

# Finishing
echo "Finishing..."
